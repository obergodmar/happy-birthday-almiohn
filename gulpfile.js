const gulp = require('gulp');
const browserSync = require('browser-sync').create();
const sass = require('gulp-sass');
const postcss = require('gulp-postcss');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const sourcemaps = require('gulp-sourcemaps');

const paths = {
    base: {
        src: 'public/.'
    },
    styles: {
        src: 'src/styles/*.scss',
        dest: 'public/css/'
    },
    script: {
        src: 'src/*.js'
    },
    html: {
        src: 'public/*.html'
    }
};

style();

function style() {
    return (
        gulp
            .src(paths.styles.src)
            .pipe(sourcemaps.init())
            .pipe(sass())
            .on('error', sass.logError)
            .pipe(postcss([autoprefixer(), cssnano()]))
            .pipe(sourcemaps.write())
            .pipe(gulp.dest(paths.styles.dest))
            .pipe(browserSync.stream())
    );
}


function watch() {
    browserSync.init({
        server: {
            baseDir: paths.base.src
        }
    });
    gulp.watch(paths.styles.src, style);
    gulp.watch(paths.html.src).on('change', browserSync.reload);
    gulp.watch(paths.script.src).on('change', browserSync.reload);
}

exports.watch = watch
