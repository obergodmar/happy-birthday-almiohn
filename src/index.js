const button = document.getElementById('button');
document.addEventListener('DOMContentLoaded', () => {
    button.style.opacity = '0';
    prepareScene();
    button.addEventListener('click', firstScript)
    button.style.display = 'block';
    delay(500).then(() => {
        button.style.opacity = '1';
    })
})

function firstScript() {
    button.style.opacity = '0';
    delay(500).then(() => {
        button.style.display = 'none';
    })
    let scene = document.getElementById('scene-1');
    delay(1000).then(() => {
        scene.style.display = 'block';
        delay(7000).then(() => {
            scene.style.display = 'none';
            scene = document.getElementById('scene-2');
            scene.style.display = 'block';
            delay(6000).then(() => {
                button.removeEventListener('click', firstScript);
                button.addEventListener('click', secondScript);
                button.style.display = 'block';
                delay(500).then(() => {
                    button.style.opacity = '1';
                })
            })
        })
    })
}

function secondScript() {
    button.style.opacity = '0';
    delay(500).then(() => {
        button.style.display = 'none';
    })
    delay(300).then(() => {
        const previousScene = document.getElementById('scene-2');
        previousScene.style.display = 'none';
    })
    delay(300).then(() => {
        const background = document.getElementById('background');
        background.style.backgroundColor = '#fdfbfb';
        delay(600).then(() => {
            let scene = document.getElementById('scene-3');
            scene.style.display = 'block';
            delay(5000).then(() => {
                scene.style.display = 'none';
                scene = document.getElementById('scene-4');
                scene.style.display = 'block';
                delay(6000).then(() => {
                    scene.style.display = 'none';
                    scene = document.getElementById('scene-5');
                    scene.style.display = 'block';
                    delay(6000).then(() => {
                        button.removeEventListener('click', secondScript);
                        button.addEventListener('click', thirdScript);
                        button.style.display = 'block';
                        delay(500).then(() => {
                            button.style.opacity = '1';
                        })
                    })
                })
            })
        })
    })
}

function thirdScript() {
    button.style.opacity = '0';
    delay(500).then(() => {
        button.style.display = 'none';
    })
    const audio = document.getElementById('music');
    audio.play();
    delay(500).then(() => {
        let scene = document.getElementById('scene-5');
        scene.style.display = 'none';
        delay(500).then(() => {
            scene = document.getElementById('scene-6');
            scene.style.display = 'block';
            delay(5000).then(() => {
                scene.style.display = 'none';
                scene = document.getElementById('scene-7');
                scene.style.display = 'block';
                delay(6000).then(() => {
                    button.removeEventListener('click', thirdScript);
                    button.addEventListener('click', fourthScript);
                    button.style.display = 'block';
                    delay(500).then(() => {
                        button.style.opacity = '1';
                    })
                })
            })
        })
    })
}

function fourthScript() {
    button.style.opacity = '0';
    delay(500).then(() => {
        button.style.display = 'none';
    })
    let scene = document.getElementById('scene-7');
    scene.style.display = 'none';
    createBalloons();
    const background = document.getElementById('background');
    background.style.justifyContent = 'flex-end';
    delay(600).then(() => {
       scene = document.getElementById('scene-8');
       scene.style.display = 'block';
       delay(4000).then(() => {
           scene.style.display = 'none';
           scene = document.getElementById('scene-9');
           scene.style.display = 'block';
           delay(6000).then(() => {
               button.removeEventListener('click', fourthScript);
               button.addEventListener('click', fifthScript);
               button.style.display = 'block';
               delay(500).then(() => {
                   button.style.opacity = '1';
               })
           })
       })
    })
}

function fifthScript() {
    button.style.opacity = '0';
    delay(500).then(() => {
        button.style.display = 'none';
    })
    let scene = document.getElementById('scene-9');
    scene.style.display = 'none';
    const cake = document.getElementById('cake');
    cake.style.display = 'block';
    delay(500).then(() => {
        cake.style.opacity = '1';
    })
    delay(4000).then(() => {
        scene = document.getElementById('scene-10');
        scene.style.display = 'block';
        delay(6000).then(() => {
            button.removeEventListener('click', fifthScript);
            button.addEventListener('click', sixthScript);
            button.style.display = 'block';
            delay(500).then(() => {
                button.style.opacity = '1';
            })
        })
    })
}

function sixthScript() {
    button.style.opacity = '0';
    delay(500).then(() => {
        button.style.display = 'none';
    })
    let scene = document.getElementById('scene-10');
    scene.style.display = 'none';
    messageFlow(11).then(() => {
        createFlyingBalloons(20);
        const fireworks = document.getElementById('fireworks');
        fireworks.style.display = 'block';
        const audio = document.getElementById('fireworks-sound');
        audio.play();
        const lastFigure = document.getElementById('last-figure');
        lastFigure.style.display = 'flex';
        delay(500).then(() => {
            lastFigure.style.opacity = '1'
        })
    });
}

function messageFlow(script) {
    return delay(4000).then(() => {
        let scene = document.getElementById(`scene-${script - 1}`);
        scene.style.display = 'none';
        if (script === 40) {
            return delay(3000);
        }
        scene = document.getElementById(`scene-${script}`);
        scene.style.display = 'block';

        return messageFlow(script + 1);
    })
}

function prepareScene() {
    const text = document.getElementsByClassName('text');
    [...text].forEach(element => {
        element.style.display = 'none';
    })
    const cake = document.getElementById('cake');
    cake.style.display = 'none';
    const fireworks = document.getElementById('fireworks');
    fireworks.style.display = 'none';

    const balloonContainer = document.getElementById('balloon-container');
    balloonContainer.style.display = 'none';
    const balloonContainerFly = document.getElementById('balloon-container-fly');
    balloonContainerFly.style.display = 'none';

    const lastFigure = document.getElementById('last-figure');
    lastFigure.style.display = 'none';

}

function createBalloons() {
    const balloonTemplate = document.getElementById('balloon');
    balloonTemplate.style.display = 'inline-block';
    const balloonContainer = document.getElementById('balloon-container');
    const row = document.getElementById('row-1');

    for (let i = 0; i < 2; i++) {
        const newBalloon = balloonTemplate.cloneNode();
        newBalloon.style.animationDuration = `${(Math.random() * 10 + 2).toString()}s`;
        row.appendChild(newBalloon);
    }
    const secondRow = row.cloneNode(true);
    balloonContainer.appendChild(secondRow);
    balloonContainer.style.display = 'flex';
    delay(500).then(() => {
        balloonContainer.style.opacity = '1';
    })
}

function createFlyingBalloons(number) {
    const balloonTemplate = document.getElementById('balloon-fly');
    const balloonContainer = document.getElementById('balloon-container-fly');
    const colors = [
        '#fdd4ce',
        '#d1a9ae',
        '#fcc6e2',
        '#c0809c',
        '#ffbec2',
        '#fce3e2',
        '#f7b2a5',
        '#f8dce1',
        '#db8f8d'
    ]
    for (let i = 0; i < number; i++) {
        const newBalloon = balloonTemplate.cloneNode();
        const colorNumber = (Math.random() * colors.length).toFixed();
        const color = colors[colorNumber];
        newBalloon.style.background = color;
        newBalloon.style.color = color;
        newBalloon.style.animationDuration = `${(Math.random() * 10 + 2).toString()}s`;
        balloonContainer.appendChild(newBalloon);
    }

    balloonContainer.style.display = 'block';
}

function delay(ms) {
    return new Promise(resolve => {
        setTimeout(resolve, ms);
    });
}
